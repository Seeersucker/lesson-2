/**
const readline = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout
});
   
readline.question('Type any number, see if it is divisble by 3/5: ', Number => {
    if (Number % 3 == 0) {
        if (Number % 5 == 0) {
            console.log(true);
        }
    } else {
        console.log(false);
    }
});
*/

const readline = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout
});
   
readline.question('Type any number, see if it is whether even or odd: ', Number => {
    if (Number % 2 == 0) {
        console.log('Even Number')
    } else {
        console.log('Odd Number');
    }
});